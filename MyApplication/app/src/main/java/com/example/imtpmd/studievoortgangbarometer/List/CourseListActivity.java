package com.example.imtpmd.studievoortgangbarometer.List;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.imtpmd.studievoortgangbarometer.Activity.OverzichtActivity;
import com.example.imtpmd.studievoortgangbarometer.Activity.DetailCourse;
import com.example.imtpmd.studievoortgangbarometer.DB.DatabaseHelper;
import com.example.imtpmd.studievoortgangbarometer.DB.DatabaseInfo;
import com.example.imtpmd.studievoortgangbarometer.MainActivity;
import com.example.imtpmd.studievoortgangbarometer.Models.Course;
import com.example.imtpmd.studievoortgangbarometer.R;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maikel on 11/03/2016.
 */
public class CourseListActivity extends MainActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ListView mListView;
    private CourseListAdapter mAdapter;
    private List<Course> courseModels = new ArrayList<>();
    DatabaseHelper dbh;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courselist);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mListView = (ListView) findViewById(R.id.my_list_view);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                             @Override
                                             public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                                 Intent startCourseListActivity = new Intent(getApplicationContext(), DetailCourse.class);
                                                 startCourseListActivity.putExtra("position" , position);
                                                 startActivity(startCourseListActivity);
                                             }
                                         }
        );

        dbh = DatabaseHelper.getHelper(getApplicationContext());
        Cursor rs = dbh.query(DatabaseInfo.CourseTables.COURSE, new String[]{"*"}, null, null, null, null, DatabaseInfo.CourseColumn.PERIOD);

        for (int i = 0; i < rs.getCount(); i++){
            rs.moveToPosition(i);
            String name = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.NAME));
            String ects = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.ECTS));
            String period = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.PERIOD));
            String grade = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.GRADE));
            int kleur = Color.TRANSPARENT;
            int text = Color.BLACK;

            if (!grade.equals("NNB")){
                Double cijfer;
                cijfer = Double.parseDouble(grade);
                if (cijfer >= 5.5){
                    //groen
                    kleur = 0xFF4CAF50;
                }
                else{
                    //red
                    kleur = 0xFFF44336;
                    text = Color.WHITE;
                }
            }


            courseModels.add(new Course(name, ects, period, grade, kleur, text));
        }
        mAdapter = new CourseListAdapter(CourseListActivity.this, 0, courseModels);
        mListView.setAdapter(mAdapter);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            //things to do
            Intent a = new Intent(CourseListActivity.this, MainActivity.class);
            a.putExtra("id", id);
            a.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(a);

        } else if (id == R.id.nav_gegevens) {
            //thing to do

        } else if (id == R.id.nav_overzicht) {
            Intent d = new Intent(CourseListActivity.this, OverzichtActivity.class);
            d.putExtra("id", id);
            d.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(d);


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Intent main = new Intent(CourseListActivity.this, MainActivity.class);
            startActivity(main);
            finish();
        }

    }

}
