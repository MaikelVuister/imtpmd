package com.example.imtpmd.studievoortgangbarometer.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.imtpmd.studievoortgangbarometer.MainActivity;
import com.example.imtpmd.studievoortgangbarometer.R;
import com.example.imtpmd.studievoortgangbarometer.SharedPreff;


public class FirstTimeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_time);

        final EditText student_nummerEdit = (EditText)findViewById(R.id.student_nummer);
        final EditText student_naamEdit = (EditText)findViewById(R.id.student_naam);


        Button button = (Button) findViewById(R.id.first_time_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // record the fact that the app has been started at least once
                SharedPreff.getSettings(getApplicationContext()).edit().putBoolean("first_time", false).commit();

                //save student naam
                String snaam = student_naamEdit.getText().toString();
                SharedPreff.getStudent(getApplicationContext()).edit().putString("student_naam", snaam).commit();

                String snummer = student_nummerEdit.getText().toString();
                SharedPreff.getStudent(getApplicationContext()).edit().putString("student_nummer", snummer).commit();

                //start homescreen
                Intent startMainActivity = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(startMainActivity);

            }
        });
    }

}
