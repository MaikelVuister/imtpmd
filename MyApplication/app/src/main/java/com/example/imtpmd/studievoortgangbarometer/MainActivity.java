package com.example.imtpmd.studievoortgangbarometer;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.imtpmd.studievoortgangbarometer.Activity.FirstTimeActivity;
import com.example.imtpmd.studievoortgangbarometer.Activity.OverzichtActivity;
import com.example.imtpmd.studievoortgangbarometer.Functions.getStudiepunten;
import com.example.imtpmd.studievoortgangbarometer.List.CourseListActivity;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //definiëren intents
    Intent a;
    Intent b;
    Intent c;

    int nietBehaald;
    int studiepunten;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (SharedPreff.getSettings(getApplicationContext()).getBoolean("first_time", true)) {
            // first time task
            Intent startFirstTimeActivity = new Intent(getApplicationContext(), FirstTimeActivity.class);
            startActivity(startFirstTimeActivity);
        }
        else {
            setContentView(R.layout.activity_main);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            //welkomstekst
            String naam = SharedPreff.getStudent(getApplicationContext()).getString("student_naam", "");
            String studentnummer = SharedPreff.getStudent(getApplicationContext()).getString("student_nummer", "");

            TextView welkom_text = (TextView) findViewById(R.id.textView_welkom);
            welkom_text.setText("Hallo " + naam + " lekker aan het studeren?");

            TextView naam_text = (TextView) findViewById(R.id.textView_naam);
            naam_text.setText(naam);

            TextView stnr_text = (TextView) findViewById(R.id.textView_stnr);
            stnr_text.setText(studentnummer);

            //periode
            java.util.Date date= new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int week = cal.get(Calendar.WEEK_OF_YEAR);
            TextView periode = (TextView) findViewById(R.id.textView_per);
            if (week >= 35 && week <= 46){
                periode.setText("Periode 1");
            }
            if (week >= 47 && week <= 6){
                periode.setText("Periode 2");
            }
            if (week >= 7 && week <= 18){
                periode.setText("Periode 3");
            }
            if (week >= 19 && week <= 29){
                periode.setText("Periode 4");
            }

            getStudiepunten gsp = new getStudiepunten();
            studiepunten = gsp.getPunten(getApplicationContext());
            nietBehaald = gsp.getOnvoldoendes(getApplicationContext());

            String punten = Integer.valueOf(studiepunten).toString();
            TextView pnt = (TextView) findViewById(R.id.textView_ec);
            pnt.setText(punten + "/60");

            Double deler = Double.valueOf(studiepunten)/(Double.valueOf(nietBehaald)+Double.valueOf(studiepunten));
            Double verwachting = 60 * deler;
            int afgerond = (int) verwachting.doubleValue();
            TextView advies = (TextView) findViewById(R.id.StudieAdvies);

            if (Double.valueOf(deler).toString().equals("NaN")){
                advies.setText("");
            }
            else {
                if (afgerond == 60) {
                    advies.setText("De verwaching is dat je " + afgerond + " punten gaat halen. Dit betekend dat je in één jaar je propedeuse kan halen! Ga zo door!!");
                    advies.setTextColor(0xFF2E7D32);
                } else if (afgerond >= 50) {
                    advies.setText("De verwaching is dat je " + afgerond + " punten gaat halen. Ga zo door kanjer! Zo als het er nu naar uit ziet is deze studie voor jou geschikt.");
                    advies.setTextColor(0xFF43A047);
                } else if (afgerond >= 40) {
                    advies.setText("De verwaching is dat je " + afgerond + " punten gaat halen. Je zult dan blijven zitten, overweeg een gesprek met je mentor");
                    advies.setTextColor(0xFFFF6F00);
                } else if (afgerond <= 40) {
                    advies.setText("De verwaching is dat je " + afgerond + " punten gaat halen. Ga op gesprek bij je mentor, en overweeg of dit wel de geschikte studie is voor jou.");
                    advies.setTextColor(0xFFF44336);
                }
            }


            if (!isOnline()){
                Snackbar.make(findViewById(android.R.id.content), "Er is geen internetverbinding!", Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.RED)
                        .show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            //thing to do
            a = new Intent(MainActivity.this, MainActivity.class);
            a.putExtra("id", id);
            a.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(a);

        } else if (id == R.id.nav_gegevens) {
            //thing to do
            b = new Intent(MainActivity.this, CourseListActivity.class);
            b.putExtra("id", id);
            b.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(b);

        } else if (id == R.id.nav_overzicht) {
            //thing to do
            c = new Intent(MainActivity.this, OverzichtActivity.class);
            c.putExtra("id", id);
            c.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(c);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}