package com.example.imtpmd.studievoortgangbarometer.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.imtpmd.studievoortgangbarometer.DB.DatabaseHelper;
import com.example.imtpmd.studievoortgangbarometer.DB.DatabaseInfo;
import com.example.imtpmd.studievoortgangbarometer.List.CourseListActivity;
import com.example.imtpmd.studievoortgangbarometer.Models.Course;
import com.example.imtpmd.studievoortgangbarometer.R;

public class DetailCourse extends AppCompatActivity {

    DatabaseHelper dbh;
    Course course;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = getIntent().getExtras();
        final int position = b.getInt("position");

        dbh = DatabaseHelper.getHelper(getApplicationContext());
        Cursor rs = dbh.query(DatabaseInfo.CourseTables.COURSE, new String[]{"*"}, null, null, null, null, DatabaseInfo.CourseColumn.PERIOD);
        rs.moveToPosition(position);
        String name     = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.NAME));
        String ects     = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.ECTS));
        String period   = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.PERIOD));
        String grade    = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.GRADE));
        int kleur    = Color.YELLOW;
        int tekst    = Color.YELLOW;
        this.course = new Course(name, ects, grade, period, kleur, tekst);

        String gehaald = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.GEHAALD));

        setContentView(R.layout.activity_detail_course);

        if (name.equals("INET") || name.equals("IRDB") || name.equals("IOPR2") || name.equals("IOPR1")){
            Snackbar.make(findViewById(android.R.id.content), "LET OP " + name +" IS EEN KERNVAK HIERVAN MOETEN ER TWEE WORDEN GEHAALD!", Snackbar.LENGTH_LONG)
                    .setActionTextColor(Color.RED)
                    .show();
        }

        Button beoordeling = (Button) findViewById(R.id.BeoordelingBTN);
        beoordeling.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText cijfertxt = (EditText) findViewById(R.id.IngevuldeBeoordeling);
                Double cijfer = 100.00;
                try {
                    cijfer = Double.parseDouble(cijfertxt.getText().toString());
                }catch (Exception e){

                }
                if (cijfer > 10 || cijfer < 1){
                    //cijfer afkeuren
                    Snackbar.make(findViewById(android.R.id.content), "VUL EEN CIJFER IN TUSSEN DE 1 EN 10!", Snackbar.LENGTH_LONG)
                            .setActionTextColor(Color.RED)
                            .show();
                }
                if (cijfer <= 10 && cijfer > 1) {
                    //cijfer opslaan

                    Snackbar.make(findViewById(android.R.id.content), "JE CIJFER "+ cijfer +" IS OPGESLAGEN", Snackbar.LENGTH_LONG)
                            .setActionTextColor(Color.RED)
                            .show();

                    String gehaald = "false";
                    if (cijfer > 5.5){
                        gehaald = "true";
                    }

                    dbh.updateRow(course.name, course.ects, Double.valueOf(cijfer).toString(), course.period, gehaald, position);

                    finish();
                    startActivity(getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                }
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtname = (TextView) findViewById(R.id.textModule);
        txtname.setText(course.name);
        TextView txtectc = (TextView) findViewById(R.id.textEC);
        txtectc.setText(course.ects);
        TextView txtgrade = (TextView) findViewById(R.id.MIJNCIJFER);
        txtgrade.setText(course.grade);
        TextView txtperiod = (TextView) findViewById(R.id.textPeriode);
        txtperiod.setText(course.period);


        if (!course.grade.equals("NNB")){
            Double cijfer;
            cijfer = Double.parseDouble(course.grade);
            if (cijfer >= 5.5){
                txtgrade.setBackgroundColor(0xFF4CAF50);
            }
            else{
                txtgrade.setBackgroundColor(0xFFF44336);
            }
        }
    }

    @Override
    public void onBackPressed(){
        Intent b;
        b = new Intent(DetailCourse.this, CourseListActivity.class);
        b.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(b);
        finish();
    }

}
