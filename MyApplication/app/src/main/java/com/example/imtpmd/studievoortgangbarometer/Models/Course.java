package com.example.imtpmd.studievoortgangbarometer.Models;

import java.io.Serializable;

/**
 * Created by Maikel on 11/03/2016.
 */
public class Course implements Serializable {           // WAAROM serializable ????

    public String name;
    public String ects;
    public String grade;
    public String period;
    public int kleur;
    public int text;


    public Course(String courseName, String ects, String grade, String period, int kleur, int text){
        this.name = courseName;
        this.ects = ects;
        this.grade = grade;
        this.period = period;
        this.kleur = kleur;
        this.text = text;
    }
}

