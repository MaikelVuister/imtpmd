package com.example.imtpmd.studievoortgangbarometer;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.imtpmd.studievoortgangbarometer.DB.DatabaseHelper;
import com.example.imtpmd.studievoortgangbarometer.DB.DatabaseInfo;
import com.example.imtpmd.studievoortgangbarometer.GSON.GsonRequest;
import com.example.imtpmd.studievoortgangbarometer.GSON.VolleyHelper;
import com.example.imtpmd.studievoortgangbarometer.Models.Course;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class Splash extends AppCompatActivity {

    DatabaseHelper dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        StartAnimations();
        Thread myThread = new Thread(){
            @Override
            public void run() {                try {
                    sleep(3000);

                    if (SharedPreff.getSettings(getApplicationContext()).getBoolean("first_time", true)) {
                        if (isOnline()) {
                            dbh = DatabaseHelper.getHelper(getApplicationContext());
                            requestSubjects();
                            Intent startMainScreen = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(startMainScreen);
                            finish();
                        } else {
                            //melding dat er internet nodig is wanneer de applicatie voor het eerst wordt gebruikt.
                            Intent firstTimeWarning = new Intent(getApplicationContext(), WarningActivity.class);
                            startActivity(firstTimeWarning);
                            finish();
                        }
                    }
                    else {
                        Intent startMainScreen = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(startMainScreen);
                        finish();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };
        myThread.start();
    }


    private void requestSubjects(){
        Type type = new TypeToken<List<Course>>(){}.getType();
        GsonRequest<List<Course>> request = new GsonRequest<List<Course>>(
                "http://fuujokan.nl/subject_lijst.json", type, null,
                new Response.Listener<List<Course>>() {
                    @Override
                    public void onResponse(List<Course> response) {
                        processRequestSucces(response);

                        if (response.size() > 0){
                            dbh.emptyTable(DatabaseInfo.CourseTables.COURSE);
                        }

                        for (int i = 0; i < response.size(); i++){

                            ContentValues values = new ContentValues();

                            String name = response.get(i).name;
                            values.put(DatabaseInfo.CourseColumn.NAME, name);

                            String ects = response.get(i).ects;
                            values.put(DatabaseInfo.CourseColumn.ECTS, ects);

                            String grade = "NNB";
                            values.put(DatabaseInfo.CourseColumn.GRADE, grade);

                            String period = response.get(i).period;
                            values.put(DatabaseInfo.CourseColumn.PERIOD, period);

                            values.put(DatabaseInfo.CourseColumn.GEHAALD, "false");

                            dbh.insert(DatabaseInfo.CourseTables.COURSE, null, values);
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                processRequestError(error);
            }
        }
        );
        VolleyHelper.getInstance(this).addToRequestQueue(request);
    }

    private void processRequestSucces(List<Course> subjects ){   }

    private void processRequestError(VolleyError error){  }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        RelativeLayout l = (RelativeLayout) findViewById(R.id.rel_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.imageView2);
        iv.clearAnimation();
        iv.startAnimation(anim);


    }


}

