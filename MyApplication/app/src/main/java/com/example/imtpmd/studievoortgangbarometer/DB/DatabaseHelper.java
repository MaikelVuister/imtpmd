package com.example.imtpmd.studievoortgangbarometer.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Maikel on 04/03/2016.
 */

// methods van de database

public class DatabaseHelper extends SQLiteOpenHelper {
    public static SQLiteDatabase mSQLDB;
    private static DatabaseHelper mInstance;
    public static final String dbName = "barometer.db";
    public static final int dbVersion = 3;

    public DatabaseHelper(Context ctx) {
        super(ctx, dbName, null, dbVersion);
    }

    public static synchronized DatabaseHelper getHelper (Context ctx){
        if (mInstance == null){
            mInstance = new DatabaseHelper(ctx);
            mSQLDB = mInstance.getWritableDatabase();
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DatabaseInfo.CourseTables.COURSE + " (" +
                        BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DatabaseInfo.CourseColumn.NAME + " TEXT," +
                        DatabaseInfo.CourseColumn.ECTS + " TEXT," +
                        DatabaseInfo.CourseColumn.GRADE + " TEXT," +
                        DatabaseInfo.CourseColumn.PERIOD + " TEXT," +
                        DatabaseInfo.CourseColumn.GEHAALD + " TEXT);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.CourseTables.COURSE);
        onCreate(db);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version ){
        super(context, name, factory, version);
    }

    public void insert(String table, String nullColumnHack, ContentValues values){
        mSQLDB.insert(table, nullColumnHack, values);
    }

    public Cursor query(String table, String[] columns, String selection, String[] selectArgs, String groupBy, String having, String orderBy){
        return mSQLDB.query(table, columns, selection, selectArgs, groupBy, having, orderBy);
    }

    public void emptyTable(String table){
        mSQLDB.execSQL("DELETE FROM " + table);
    }

    public void updateRow(String name, String ects, String grade, String period, String gehaald, int position) {
        ContentValues args = new ContentValues();
        args.put(DatabaseInfo.CourseColumn.NAME, name);
        args.put(DatabaseInfo.CourseColumn.ECTS, ects);
        args.put(DatabaseInfo.CourseColumn.GRADE, grade);
        args.put(DatabaseInfo.CourseColumn.PERIOD, period);
        args.put(DatabaseInfo.CourseColumn.GEHAALD, gehaald);

        mSQLDB.update(DatabaseInfo.CourseTables.COURSE, args, DatabaseInfo.CourseColumn.NAME + "=" + "'"+name+"'", null);
    }

}