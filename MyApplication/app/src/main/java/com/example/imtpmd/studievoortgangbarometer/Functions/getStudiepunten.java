package com.example.imtpmd.studievoortgangbarometer.Functions;

import android.content.Context;
import android.database.Cursor;

import com.example.imtpmd.studievoortgangbarometer.DB.DatabaseHelper;
import com.example.imtpmd.studievoortgangbarometer.DB.DatabaseInfo;

/**
 * Created by Maikel on 15/04/2016.
 */
public class getStudiepunten {

    DatabaseHelper dbh;

    public int getPunten(Context c) {
        int studiepunten = 0;

        dbh = DatabaseHelper.getHelper(c);
        String whereclause = DatabaseInfo.CourseColumn.GEHAALD + " = 'true'";
        Cursor rs = dbh.query(DatabaseInfo.CourseTables.COURSE, new String[]{"*"}, whereclause, null, null, null, DatabaseInfo.CourseColumn.PERIOD);

        for (int i = 0; i < rs.getCount(); i++){
            rs.moveToPosition(i);
            String punten = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.ECTS));
            studiepunten = studiepunten + Integer.parseInt(punten);
        }

        return studiepunten;
    }

    public int getOnvoldoendes(Context c){
        int nietBehaald = 0;

        dbh = DatabaseHelper.getHelper(c);
        String whereclause = DatabaseInfo.CourseColumn.GEHAALD + " = 'false' AND " + DatabaseInfo.CourseColumn.GRADE + "!= 'NNB'";
        Cursor rs = dbh.query(DatabaseInfo.CourseTables.COURSE, new String[]{"*"}, whereclause, null, null, null, DatabaseInfo.CourseColumn.PERIOD);

        for (int i = 0; i < rs.getCount(); i++){
            rs.moveToPosition(i);
            String punten = rs.getString(rs.getColumnIndex(DatabaseInfo.CourseColumn.ECTS));
            nietBehaald = nietBehaald + Integer.parseInt(punten);
        }

        return nietBehaald;
    }

}
