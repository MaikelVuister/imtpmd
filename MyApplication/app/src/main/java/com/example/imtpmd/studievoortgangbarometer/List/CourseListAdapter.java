package com.example.imtpmd.studievoortgangbarometer.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.imtpmd.studievoortgangbarometer.Models.Course;
import com.example.imtpmd.studievoortgangbarometer.R;

import java.util.List;

/**
 * Created by Maikel on 11/03/2016.
 */
public class CourseListAdapter extends ArrayAdapter<Course> {

    public CourseListAdapter(Context context, int resource, List<Course> objects){
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        Course cm = getItem(position);
        if (convertView == null ) {
            vh = new ViewHolder();
            LayoutInflater li = LayoutInflater.from(getContext());
            convertView = li.inflate(R.layout.view_content_row, parent, false);
            vh.name = (TextView) convertView.findViewById(R.id.subject_name);
            vh.code = (TextView) convertView.findViewById(R.id.subject_code);
            vh.grade = (TextView) convertView.findViewById(R.id.subject_code4);

            vh.gradeT = (TextView) convertView.findViewById(R.id.subject_code2);
            vh.nameT = (TextView) convertView.findViewById(R.id.subject_name2);
            vh.codeT = (TextView) convertView.findViewById(R.id.subject_code3);

            convertView.setTag(vh);


        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        vh.name.setText(cm.name);
        vh.code.setText(cm.ects);
        vh.grade.setText(cm.period);

        convertView.setBackgroundColor(cm.kleur);

        vh.grade.setTextColor(cm.text);
        vh.code.setTextColor(cm.text);
        vh.name.setTextColor(cm.text);
        vh.gradeT.setTextColor(cm.text);
        vh.codeT.setTextColor(cm.text);
        vh.nameT.setTextColor(cm.text);

        return convertView;
    }

    private static class ViewHolder {
        TextView name;
        TextView code;
        TextView grade;
        TextView nameT;
        TextView codeT;
        TextView gradeT;
    }
}