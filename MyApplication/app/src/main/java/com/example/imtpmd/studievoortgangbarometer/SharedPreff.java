package com.example.imtpmd.studievoortgangbarometer;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Maikel on 11/03/2016.
 */
public class SharedPreff {

    private static Context mContext;

    //Strings
    private static String PREFS_settings = "MySettings";
    private static String PREFS_student = "MyStudent";

    //getters Strings
    public static String getPREFS_settings(){
        return PREFS_settings;
    }

    public static String getPREFS_student(){
        return PREFS_student;
    }

    //getters prefs
    public static SharedPreferences getSettings(Context ctxt){
        return ctxt.getSharedPreferences(getPREFS_settings(), 0);
    }

    public static SharedPreferences getStudent(Context ctxt){
        return ctxt.getSharedPreferences(getPREFS_student(),0 );
    }

}
