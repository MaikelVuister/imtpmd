package com.example.imtpmd.studievoortgangbarometer.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.imtpmd.studievoortgangbarometer.Functions.getStudiepunten;
import com.example.imtpmd.studievoortgangbarometer.List.CourseListActivity;
import com.example.imtpmd.studievoortgangbarometer.MainActivity;
import com.example.imtpmd.studievoortgangbarometer.R;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;

public class OverzichtActivity extends MainActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private PieChart mChart;
    public static final int MAX_ECTS = 60;
    public static int currentEcts = 0;
    int aantalPunten;
    int nietBehaald;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overzicht);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mChart = (PieChart) findViewById(R.id.chart);
        mChart.setDescription("");
        mChart.setTouchEnabled(false);
        mChart.setDrawSliceText(true);
        mChart.getLegend().setEnabled(false);
        mChart.setTransparentCircleColor(Color.rgb(130, 130, 130));
        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        getStudiepunten gsp = new getStudiepunten();
        aantalPunten = gsp.getPunten(getApplicationContext());
        nietBehaald = gsp.getOnvoldoendes(getApplicationContext());

        setData(0);
    }

    private void setData(int aantal) {
        currentEcts = aantalPunten;
        ArrayList<Entry> yValues = new ArrayList<>();
        ArrayList<String> xValues = new ArrayList<>();

        if (currentEcts != 0) {
            yValues.add(new Entry(currentEcts, 0));
            xValues.add("Behaalde studiepunten");
        }

        if (nietBehaald != 0){
            yValues.add(new Entry(nietBehaald, 1));
            xValues.add("Onbehaalde studiepunten");

        }

        if (60 - currentEcts - nietBehaald != 0) {
            yValues.add(new Entry(60 - currentEcts - nietBehaald, 2));
            xValues.add("Resterende studiepunten");
        }
        //  http://www.materialui.co/colors
        ArrayList<Integer> colors = new ArrayList<>();
        if (currentEcts < 10) {
            colors.add(Color.rgb(200, 230, 201));
        } else if (currentEcts < 40) {
            colors.add(Color.rgb(129, 199, 132));
        } else if (currentEcts < 50) {
            colors.add(Color.rgb(102, 187, 106));
        } else {
            colors.add(Color.rgb(76, 175, 80));
        }
        colors.add(Color.rgb(244, 67, 54));
        colors.add(Color.rgb(255, 235, 59));

        PieDataSet dataSet = new PieDataSet(yValues, "ECTS");
        dataSet.setColors(colors);

        PieData data = new PieData(xValues, dataSet);
        mChart.setData(data); // bind dataset aan chart.
        mChart.invalidate();  // Aanroepen van een redraw
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Intent main = new Intent(OverzichtActivity.this, MainActivity.class);
            startActivity(main);
            finish();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            //things to do
            Intent a = new Intent(OverzichtActivity.this, MainActivity.class);
            a.putExtra("id", id);
            a.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(a);

        } else if (id == R.id.nav_gegevens) {
            //thing to do
            Intent b = new Intent(OverzichtActivity.this, CourseListActivity.class);
            b.putExtra("id", id);
            b.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(b);

        } else if (id == R.id.nav_overzicht) {
            //thing to do

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}