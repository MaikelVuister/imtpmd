package com.example.imtpmd.studievoortgangbarometer.DB;

/**
 * Created by Maikel on 04/03/2016.
 */

//Namen van de database en waar de tabellen staan

public class DatabaseInfo {

    public class CourseTables{
        public static final String COURSE = "course";
    }

    public class CourseColumn{
        public static final String NAME = "name";
        public static final String ECTS = "ects";
        public static final String GRADE = "grade";
        public static final String PERIOD = "period";
        public static final String GEHAALD = "false";
    }

}
